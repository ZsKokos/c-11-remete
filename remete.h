#include <array>
#include <memory>
#include <unordered_map>
#include <algorithm>
#include <future>
#include <fstream>
#include <thread>
#include <sstream>

#pragma once

namespace remete {
	enum class direction_t {
				x_inc,
				x_dec,
				y_inc,
				y_dec,
				direction_max
	};
	std::string to_string(direction_t d) {
		static const std::unordered_map<direction_t, std::string> DirectionMap{
			{direction_t::x_inc, "X+"},
			{direction_t::x_dec, "X-"},
			{direction_t::y_inc, "Y+"},
			{direction_t::y_dec, "Y-"},
		};
		return DirectionMap.at(d);
	}
	class table
	{
		enum slot { x, o, c, };
		static char to_char(slot s) {
			static const std::unordered_map<slot, char> SlotMap {
				{slot::x, ' '},
				{slot::o, 'o'},
				{slot::c, '.'},
			};
			return SlotMap.at(s);
		};
	public:
		static constexpr size_t width	= sizeof("xxoooxx")-1;
		static constexpr size_t height	= sizeof("xxoooxx")-1;

		using matrix_row = std::array<slot, width>;
		using matrix = std::array<matrix_row, height>;
	private:
		matrix t;
	private:
		std::ostream& render(std::ostream& stream) const
		{
			stream << std::endl;
			std::for_each( t.begin(), t.end(), [&stream](matrix_row row) { 
				std::for_each( row.begin(), row.end(), [&stream](slot s) {
					stream << to_char(s);
				});
				stream << std::endl;
				} );
			return stream << std::endl;
		};
	public:
		table(const matrix &slots) : t{slots} {};
	public:
		static constexpr matrix classic_startup {{
			{x,x,o,o,o,x,x,},
			{x,x,o,o,o,x,x,},
			{o,o,o,o,o,o,o,},
			{o,o,o,c,o,o,o,},
			{o,o,o,o,o,o,o,},
			{x,x,o,o,o,x,x,},
			{x,x,o,o,o,x,x,},
		}};
		static constexpr matrix classic_winner {{
			{x,x,c,c,c,x,x,},
			{x,x,c,c,c,x,x,},
			{c,c,c,c,c,c,c,},
			{c,c,c,o,c,c,c,},
			{c,c,c,c,c,c,c,},
			{x,x,c,c,c,x,x,},
			{x,x,c,c,c,x,x,},
		}};
		
		//using slot_iterator = std::pair<matrix::iterator,matrix_row::iterator>;
		using slot_iterator = std::pair<size_t,size_t>;
		using step = std::pair<slot_iterator,slot_iterator>;
		class invalid_step : public std::exception {};
		void validate_step(bool condition, std::string what) const { if(!condition) throw invalid_step(); }
		bool operator==(const table& _t) const { return _t.t == t; };
		bool is_left_rotation_of(const table& _t) const
		{
			for( size_t x = 0; x < width; x++ )
				for( size_t y = 0; y < height; y++ )
					if( t[x][y] != _t.t[height-y-1][x] ) return false;
			return true;
		}
		bool is_right_rotation_of(const table& _t) const
		{
			for( size_t x = 0; x < width; x++ )
				for( size_t y = 0; y < height; y++ )
					if( t[x][y] != _t.t[y][height-x-1] ) return false;
			return true;
		}
		bool is_point_reflection_of(const table& _t) const
		{
			for( size_t x = 0; x < width; x++ )
				for( size_t y = 0; y < height; y++ )
					if( t[x][y] != _t.t[height-x-1][height-y-1] ) return false;
			return true;
		}
		bool is_equivalent (const table& _t) const 
		{
			if( *this == _t ) return true;
			if( is_left_rotation_of(_t) ) return true;
			if( is_right_rotation_of(_t) ) return true;
			if( is_point_reflection_of(_t) ) return true;
			return false;
		}

		//bool operator==(const table& _t) const { return std::equal(_t,t); };
		void validate_step(const step st) const
		{
			const slot_iterator& move_from = st.first;
			const slot_iterator& move_to = st.second;
			const slot_iterator move_middle((move_to.first+move_from.first)/2,(move_to.second+move_from.second)/2);
//			std::cout << "Validating (" << move_from.first << "," << move_from.second << ") - ("<< move_to.first <<","<< move_to.second <<")";
			validate_step( (move_to.first == move_from.first) || (move_to.second == move_from.second), "across/down" );
			validate_step( (move_from.first==move_to.first+2) || (move_to.first==move_from.first+2) || (move_from.second==move_to.second+2) || (move_to.second==move_from.second+2), "distance:2" );
			validate_step( o == t.at(move_from.first).at(move_from.second), "from:o" );
			validate_step( o == t.at(move_middle.first).at(move_middle.second), "middle:o" );
			validate_step( c == t.at(move_to.first).at(move_to.second), "target:c" );
//			std::cout << "Valid step found!";
		};
		void do_step(const step st)
		{
			const slot_iterator& move_from = st.first;
			const slot_iterator& move_to = st.second;
			const slot_iterator move_middle((move_to.first+move_from.first)/2,(move_to.second+move_from.second)/2);
			validate_step(st);
			t[move_from.first][move_from.second] = c;
			t[move_middle.first][move_middle.second] = c;
			t[move_to.first][move_to.second] = o;
			
		};
		friend std::ostream& operator<< (std::ostream& stream, const table& _table) { return _table.render(stream); };
	};
	constexpr remete::table::matrix remete::table::classic_startup, remete::table::classic_winner;

	class state{
	protected:
		table _t;
		state(const table &t) : _t{t}, level{0} {};
		const size_t level;
		//virtual std::ostream& operator<< (std::ostream& stream) const { return stream << level << _t; };
	public:
		virtual std::string branches() const = 0;
		bool is_threadable() const { return level < 2; }
		bool is_high_level() const { return level >= 31; };
		state(const state &s, const table::step st) : _t{s._t}, level{s.level+1} { _t.do_step(st); }
		friend std::ostream& operator<< (std::ostream& stream, const state& _state) {
			return stream << _state.level << _state.branches() << _state._t;
			//return stream << _state;
		};
		bool is_table(const table& t) const { return _t == t; };
	};
	class initial_state : public state {
	public:
		virtual std::string branches() const { return std::string(""); };
		initial_state(const table &t) : state{t} {};
	};
	class child_state : public state {
		const state &previous;
		const table::step &step;
		static size_t count;
		const direction_t direction;
		//virtual std::ostream& operator<< (std::ostream& stream) const { return stream << step.first.first << step.first.second << step.second.first << step.second.second; };
	protected:
	public:
		virtual std::string branches() const { return previous.branches().append(" ").append(std::to_string(step.first.first)).append(std::to_string(step.first.second)).append(to_string(direction)); };
		child_state(const state& s, table::step st, direction_t dir) : state(s,st), previous{s}, step(st), direction{dir}
		{
			// std::cout << ++count << ' ';
		};
		~child_state()
		{
			// std::cout << --count << ' ';
		}
	};
	size_t child_state::count = 0;
	
	class game
	{
		const initial_state t0{table::classic_startup};
		class solution_found : public std::exception {};
		bool do_this_step (const state &t, size_t x0, size_t y0, size_t x1, size_t y1, direction_t direction) const
		{
			//constexpr ptrdiff_t xdiff = ((direction+3)%4+1)/4*4 + direction*2%4 - 2;
			// Expected: rightwards(0):+2 downwards(1):0  leftwards(2):-2 upwards(3):0
			//constexpr ptrdiff_t ydiff = ((direction+2)%4+1)/4*4 + (direction+1)*2%4 - 2;
			// Expected: rightwards(0):0  downwards(1):+2 leftwards(2):0  upwards(3):-2
			//constexpr size_t x1 = x0 + xdiff, y1 = y0 + ydiff;
			try
			{
				const table::step s(remete::table::slot_iterator(x0,y0),remete::table::slot_iterator(x1,y1));
				const child_state cs(t,s,direction);
				//outfile << std::right << std::this_thread::get_id() << *cs;
				if(!do_all_possible_steps(cs) && cs.is_high_level() )
				{
					std::ostringstream os; os << cs;
					std::cout << os.str();
				}
				//if( cs->is_table(table::classic_winner) ) throw solution_found(cs);
			}
			catch (table::invalid_step) { return false; }
			return true;
		};
		bool validate_this_step (const state &cs, size_t x0, size_t y0, direction_t d) const
		{
			using remete::direction_t;
			switch (d)
			{
				case remete::direction_t::x_inc: return (x0 + 2 < table::width) ? do_this_step(cs,x0,y0,x0 + 2,y0,d) : false;
				case remete::direction_t::x_dec: return (x0 >= 2) ? do_this_step(cs,x0,y0,x0 - 2,y0,d) : false;
				case remete::direction_t::y_inc: return (y0 + 2 < table::height) ? do_this_step(cs,x0,y0,x0,y0 + 2,d) : false;
				case remete::direction_t::y_dec: return (y0 >= 2) ? do_this_step(cs,x0,y0,x0,y0 - 2,d) : false;
				default: return false;
			}
		}
	public:
		const auto do_step(table::step s) { return std::make_unique<child_state>(t0,s, direction_t::x_dec); };
		bool do_all_possible_steps(const state &initiating_state) const
		{
			//using step_task = std::packaged_task<bool(const state &, int, size_t, size_t)>;
			using step_task = std::future<bool>;
			//using slot_t = std::unordered_map< remete::direction_t, step_task>;
			using slot_t = std::array< step_task, static_cast<size_t>(remete::direction_t::direction_max)>;
			using row_t = std::array< slot_t, table::height>;
			using tabletasks_t = std::array< row_t, table::width>;
			tabletasks_t tasks;
			for ( auto &row : tasks )
				for ( auto &slot : row )
					for ( auto &task : slot )
					{
						const size_t x0 = (&row - &tasks[0]), y0 = (&slot - &row[0]);
						//const direction_t direction = task.first;
						const direction_t direction = static_cast<direction_t>(&task - &slot[0]); // TODO: direction_t indexed std::map?
						const auto policy = initiating_state.is_threadable() ? std::launch::async : std::launch::deferred;
						//static std::atomic<size_t> thread_count = 0;
						//if( l & std::launch::async ) std::cout << "new thread: " << ++thread_count << std::endl;
						task = std::async(policy,
							&game::validate_this_step, this,
							std::ref(initiating_state), x0, y0, direction );
					}
					/*
					for(size_t x1 = 0; x1 < table::width; x1++ )
						for(size_t y1 = 0; y1 < table::height; y1++ )
							do_this_step(x0,y0,x1,y1);
						*/
			bool has_child = false;
			for ( auto &row : tasks )
				for ( auto &slot : row )
					for ( auto &task : slot )
						//if(task.get_future().get()) return true; // at least one child
						if(task.get()) has_child = true; // at least one child
			return has_child; // no children, dead branch
		}
		void do_all_possible_steps_recursive() {
			do_all_possible_steps(t0);
		};
	};

}

