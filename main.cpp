#include <iostream>
#include "remete.h"

int main()
{
	std::cout << "std::thread::hardware_concurrency(): " << std::thread::hardware_concurrency() << std::endl;
	using s = remete::table::slot_iterator;
	remete::game g;
	g.do_all_possible_steps_recursive();
	try {
		const remete::table::step s0(s(5,3),s(3,3));
		//std::cout << *g.do_step( s0 ).get();
	}
	catch (std::exception& e)
	{
		std::cout << e.what();
	}
	return 0;
}